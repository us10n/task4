package com.example.task4.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.task4.repository.Repository
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.await
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private val atmMachineCoordsLiveData = MutableLiveData<List<LatLng>>()
    val atmMachineCoords: LiveData<List<LatLng>>
        get() = atmMachineCoordsLiveData

    fun findAllAtmCoordinates(cityName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val atmMachineList = repository.getAllAtms(cityName).await()
            val atmMachinesCoords = mutableListOf<LatLng>()
            atmMachineList.forEach {
                val position = LatLng(it.gps_x.toDouble(), it.gps_y.toDouble())
                atmMachinesCoords.add(position)
            }
            atmMachineCoordsLiveData.postValue(atmMachinesCoords)
        }
    }

}