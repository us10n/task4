package com.example.task4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.task4.databinding.ActivityMapsBinding
import com.example.task4.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private val binding by lazy { ActivityMapsBinding.inflate(layoutInflater) }
    private val mainViewModel: MainViewModel by viewModels()

    companion object {
        private const val CITY_NAME = "Гомель"
        private const val ATM_TITLE = "Беларусбанк"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        mainViewModel.atmMachineCoords.observe(this) {
            showAtmMachines(it)
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val gomelCity = LatLng(52.43126701920474, 30.99403017043202)
        val zoomLevel = 12f
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, zoomLevel))

        mainViewModel.findAllAtmCoordinates(CITY_NAME)
    }

    private fun showAtmMachines(atmCoordinates: List<LatLng>) {
        atmCoordinates.forEach {
            mMap?.addMarker(MarkerOptions().position(it).title(ATM_TITLE))
        }
    }

    override fun onStop() {
        mainViewModel.atmMachineCoords.removeObservers(this)
        super.onStop()
    }
}