package com.example.task4.repository

import com.example.task4.api.BankApi
import javax.inject.Inject

class Repository @Inject constructor(
    private val bankApi: BankApi
) {
    fun getAllAtms(cityName: String) = bankApi.getAllATMMachines(cityName)
}