package com.example.task4.api

import com.example.task4.api.entity.AtmMachine
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BankApi {

    @GET("./atm")
    fun getAllATMMachines(@Query("city") cityName: String): Call<List<AtmMachine>>
}